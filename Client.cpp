#include "Client.h"
#include <cstdlib>
#include <thread>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string.h>
#include "osrng.h"
#include "des.h"
#include "modes.h"
#include <rsa.h>
#include <time.h>


using namespace CryptoPP;
using namespace  std;

client_type client = { INVALID_SOCKET, -1, "" };
string globalDecryptedAnswer = "";
CryptoDevice cryptoDevice;










int process_client(client_type &new_client)
{
	
	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{
			
			

			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			std::string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
			std::string prefix = strMessage.substr(0, position);
			std::string postfix = strMessage.substr(position);
			std::string decrypted_message;
			bool all_Ready_send_public_key = false;
			//this is the only notification we use right now :(
			if (postfix != "Disconnected") {
				//please decrypt this part!
				postfix = cryptoDevice.decryptAES(postfix);
				decrypted_message = postfix;
				
				if (cryptoDevice.encryptMD5(decrypted_message) == globalDecryptedAnswer && !all_Ready_send_public_key)
				{
					/*string str;
					int iResult = ::send(client.socket, sercetAnswer.c_str(), sercetAnswer.length(), 0);
					if (iResult <= 0)
					{
						std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
					}*/
				}


			}
			else
			{
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;
			}

			if (iResult != SOCKET_ERROR)
			{
				std::cout << prefix + postfix << std::endl;
			}
			else
			{
				std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
		std::cout << "The server has disconnected" << std::endl;

	return 0;
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	std::string sent_message = "";
	int iResult = 0;
	std::string message;

	std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	std::cout << "Connecting...\n";
	//the secret name is: "daniel"
	//the secret password is: "himsani"

	std::string input_name = "";
	std::string input_password = "";
	int trys = 4;//if the client wrong 3 times in the password and name, he kicked out

	std::cout << "Register secretly!" << std::endl;
	bool isHacker = false;

	for (int i = 0; i < trys; i++)
	{
		std::cout << "Enter name: ";
		cin >> input_name;
		std::cout << "Enter Password: ";
		cin >> input_password;

		std::string md5_name = cryptoDevice.encryptMD5(input_name);
		std::string md5_password = cryptoDevice.encryptMD5(input_password);

		std::transform(md5_name.begin(), md5_name.end(), md5_name.begin(), ::tolower);
		std::transform(md5_password.begin(), md5_password.end(), md5_password.begin(), ::tolower);



		//std::cout << cryptoDevice.encryptMD5(input_name) << " end " << cryptoDevice.encryptMD5(input_password) << std::endl;


		if (md5_name == NAME && md5_password == PASSWORD)
		{
			std::cout << "Agent accepted. walcome in!" << std::endl;
			i = trys;
		}
		else
		{
			std::cout << "Wrong PassWord and Name - try again" << std::endl;
			if (i == trys - 1) isHacker = true;
		}
	}
	if (isHacker)
	{
		return 0;
	}
	std::cout << "Successfully Connected" << std::endl;
	getchar();

	/*bool wantToChat = false;
	string option = "";

	string secretquestion = "";
	string sercetAnswer = "";

	string decryptedAnswer = "";

	cout << "Do you want to open a private chat? (y\n)" << endl;
	cin >> option;
	if (option == "y")
	{
		cout << "Enter secret Question:";
		cin >> secretquestion;
		cout << "Enter secret Answer:";
		cin >> sercetAnswer;
		decryptedAnswer = cryptoDevice.encryptMD5(sercetAnswer);
		globalDecryptedAnswer = decryptedAnswer;
		iResult = ::send(client.socket, sercetAnswer.c_str(), sercetAnswer.length(), 0);
		if (iResult <= 0)
		{
			std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
		}
	}*/
	








	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		std::cout << "Unable to connect to server!" << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}


	//Obtain id from server for this client;
	::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = std::atoi(client.received_message);

		std::thread my_thread(process_client, std::ref(client));

		while (1)
		{
			std::getline(std::cin, sent_message);

			//top secret! please encrypt
			std::string cipher = cryptoDevice.encryptAES(sent_message);

			iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		std::cout << client.received_message << std::endl;

	std::cout << "Shutting down socket..." << std::endl;
	iResult = ::shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	::closesocket(client.socket);
	::WSACleanup();
	std::system("pause");
	return 0;
}