#pragma once
#pragma once

#include "CryptoDevice.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>

using namespace std;

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512            
#define IP_ADDRESS "127.0.0.1"
#define DEFAULT_PORT "8202"

#define NAME "aa47f8215c6f30a0dcdb2a36a9f4168e" // daniel
#define PASSWORD "ae267f11359bfe76bbef214ad8dd5e13" // himsani





struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};


int process_client(client_type &new_client);