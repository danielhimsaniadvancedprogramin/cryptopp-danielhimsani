#include "CryptoDevice.h"
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string.h>
#include "osrng.h"
#include "des.h"
#include "modes.h"
#include <md5.h>
#include <hex.h>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

std::string CryptoDevice::encryptAES(std::string plainText)
{
	std::string cipherText;

	CryptoPP::AES::Encryption aesEncryption(this->key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	//  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

std::string CryptoDevice::decryptAES(std::string cipherText)
{

	std::string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(this->key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	//  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfDecryptor.Put(reinterpret_cast<const CryptoPP::byte*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}

std::string CryptoDevice::encryptMD5(std::string plainText)
{
	std::string decryptedText;

	CryptoPP::byte digets[CryptoPP::Weak1::MD5::DIGESTSIZE];
	CryptoPP::Weak1::MD5 hash;
	hash.CalculateDigest(digets, (const CryptoPP::byte*)plainText.c_str(), plainText.length());

	CryptoPP::HexEncoder encoder;
	encoder.Attach(new CryptoPP::StringSink(decryptedText));
	encoder.Put(digets, sizeof(digets));
	encoder.MessageEnd();

	return decryptedText;
}